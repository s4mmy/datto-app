package com.vaadin.tutorial.crm.ui;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.tutorial.crm.entity.Device;
import com.vaadin.tutorial.crm.service.DeviceService;

import java.util.List;

@Route("")
@CssImport(value = "./styles/text-field-styles.css")
public class MainView extends VerticalLayout {

    private final DeviceService deviceService;

    private Grid<Device> deviceGrid = new Grid<>(Device.class);
    private TextField filterText = new TextField();

    public MainView(DeviceService deviceService) {
        this.deviceService = deviceService;

        setSizeFull();
        configureFilter();
        configureGrid();

        add(filterText, deviceGrid);
        updateGrid();
    }

    private void configureFilter() {
        filterText.addThemeName("bordered");
        filterText.setPlaceholder("Search...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateGrid());
    }

    private void configureGrid() {
        deviceGrid.setSizeFull();
        deviceGrid.setColumns("uid", "profile", "hostname", "description",
                "ipAddress", "extIpAddress", "lastUser",
                "agentVersion", "model", "operatingSystem",
                "serialNumber", "motherboard", "customField1",
                "customField2", "customField3", "customField4",
                "customField5");
        deviceGrid.getColumns().forEach(col -> col.setAutoWidth(true));
    }

    private void updateGrid() {
        deviceGrid.setItems(deviceService.search(filterText.getValue()));
    }
}
