package com.vaadin.tutorial.crm.service;

import com.vaadin.tutorial.crm.entity.Device;
import com.vaadin.tutorial.crm.repository.DeviceRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@AllArgsConstructor
public class DeviceService {
    private DeviceRepository deviceRepository;

    public List<Device> search(String filterText) {
        if (StringUtils.isBlank(filterText)) {
            return deviceRepository.findAll();
        }
        return deviceRepository.search(filterText);
    }
}
