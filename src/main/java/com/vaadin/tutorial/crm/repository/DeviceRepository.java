package com.vaadin.tutorial.crm.repository;

import com.vaadin.tutorial.crm.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DeviceRepository extends JpaRepository<Device, Long> {

    @Query("select d from Device d " +
            "where lower(d.uid) like lower(concat('%', :searchTerm, '%')) " +
            "or lower(d.profile) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.hostname) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.description) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.ipAddress) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.extIpAddress) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.lastUser) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.agentVersion) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.model) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.operatingSystem) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.serialNumber) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.motherboard) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.customField1) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.customField2) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.customField3) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.customField4) like lower(concat('%', :searchTerm, '%'))" +
            "or lower(d.customField5) like lower(concat('%', :searchTerm, '%'))")
    List<Device> search(@Param("searchTerm") String searchTerm);
}
