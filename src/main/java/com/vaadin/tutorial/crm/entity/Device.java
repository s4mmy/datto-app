package com.vaadin.tutorial.crm.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String uid;

    private String profile;

    private String hostname;

    private String description;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "ext_ip_address")
    private String extIpAddress;

    @Column(name = "last_user")
    private String lastUser;

    @Column(name = "agent_version")
    private String agentVersion;

    private String model;

    @Column(name = "operating_system")
    private String operatingSystem;

    @Column(name = "serial_number")
    private String serialNumber;

    private String motherboard;

    @Column(name = "custom_field_1")
    private String customField1;

    @Column(name = "custom_field_2")
    private String customField2;

    @Column(name = "custom_field_3")
    private String customField3;

    @Column(name = "custom_field_4")
    private String customField4;

    @Column(name = "custom_field_5")
    private String customField5;
}
