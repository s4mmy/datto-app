package com.vaadin.tutorial.crm.service;

import com.vaadin.tutorial.crm.repository.DeviceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DeviceServiceTest {
    @Mock
    private DeviceRepository deviceRepository;

    private DeviceService deviceService;

    @BeforeEach
    public void setup() {
        deviceService = new DeviceService(deviceRepository);
    }

    @Test
    public void search_whenBlankFilter_returnAllDevices() {
        deviceService.search("");
        verify(deviceRepository, times(1)).findAll();
    }

    @Test
    public void search_whenNullFilter_returnAllDevices() {
        deviceService.search(null);
        verify(deviceRepository, times(1)).findAll();
    }

    @Test
    public void search_whenFilterText_filtersDevices() {
        String filterText = "test";
        deviceService.search(filterText);
        verify(deviceRepository, times(1)).search(filterText);
    }
}